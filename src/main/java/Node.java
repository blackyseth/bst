
public class Node {
    private int value;
    private Node lson = null;
    private Node rson = null;
    private Node parent = null;

    public Node(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getLson() {
        return lson;
    }

    public void setLson(Node lson) {
        this.lson = lson;
    }

    public Node getRson() {
        return rson;
    }

    public void setRson(Node rson) {
        this.rson = rson;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }
}

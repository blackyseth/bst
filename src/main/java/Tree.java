
public class Tree {
    private int root;
    private Node rootNode;

    public Tree(int root) {
        this.root = root;
        this.rootNode = new Node(root);
    }

    public void add(int number){
        Node actual = rootNode;
        Node parent = null;
        while(actual!=null){
            parent = actual;
            actual = (actual.getValue()>number)?actual.getLson():actual.getRson();
        }
        if(parent.getValue()>number){
            parent.setLson(new Node(number));
        }else{
            parent.setRson(new Node(number));
        }
    }

}
